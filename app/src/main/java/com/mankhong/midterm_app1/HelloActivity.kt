package com.mankhong.midterm_app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mankhong.midterm_app1.databinding.ActivityHelloBinding
import com.mankhong.midterm_app1.databinding.ActivityMainBinding

class HelloActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHelloBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        binding = ActivityHelloBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val name = intent?.extras?.getSerializable("Name").toString()
        binding.textShowname.text = name

    }
}